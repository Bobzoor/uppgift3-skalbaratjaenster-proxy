package org.example.Proxy;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.example.Client.ResponseInitializer;

public class ReverseProxyHandler extends SimpleChannelInboundHandler<ByteBuf> {

    private final ReverseProxyServer server;
    private Channel channel;

    public ReverseProxyHandler(ReverseProxyServer server) {
        this.server = server;
    }


/*
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {

    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        channel.close();
    }
*/


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.channel().close();
        channel.close();
    }


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf buf) throws Exception {

        var bootstrap = new Bootstrap();

        var node = server.getNodeHandler().next();

        try {
            this.channel = bootstrap
                    .group(server.getWorkerGroup())
                    .channel(NioSocketChannel.class)
                    .handler(new ResponseInitializer(node, ctx.channel()))
                    .connect("localhost", node.getPort())
                    .sync()
                    .channel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        channel.writeAndFlush(buf.copy());

    }
}
