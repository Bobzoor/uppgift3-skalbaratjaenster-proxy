package org.example.Client;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.example.node.Node;

public class ResponseHandler extends SimpleChannelInboundHandler<ByteBuf> {

    private final Node node;
    private final Channel channel;

    public ResponseHandler(Node node, Channel channel) {
        this.node = node;
        this.channel = channel;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        node.addConnection(ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        node.removeConnection(ctx.channel());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.channel().close();
        channel.close();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf buf) throws Exception {
        channel.writeAndFlush(buf.copy());
        node.addRequest();
        System.out.println("Node: " + node.getPort() + " Requests: " + node.getRequests() + " Connections: " + node.getConnections().size());
    }
}
