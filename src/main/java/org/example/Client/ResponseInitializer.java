package org.example.Client;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import org.example.node.Node;

public class ResponseInitializer extends ChannelInitializer<SocketChannel> {

    private final Node node;
    private final Channel clientChannel;

    public ResponseInitializer(Node node, Channel channel) {
        this.clientChannel = channel;
        this.node = node;
    }

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        var pipeline = channel.pipeline();

        pipeline.addLast(new ResponseHandler(node, clientChannel));
    }
}
