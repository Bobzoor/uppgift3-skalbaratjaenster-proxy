package org.example.Balancer;

import org.example.node.Node;

import java.util.List;
import java.util.Random;

public class RandomBalancer implements LoadBalancer {

    Random random = new Random();
    @Override
    public Node next(List<Node> nodes) {
        if (nodes.isEmpty())
            throw new RuntimeException("No nodes active.");

        int index = random.nextInt(nodes.size());
        return nodes.get(index);
    }


}
