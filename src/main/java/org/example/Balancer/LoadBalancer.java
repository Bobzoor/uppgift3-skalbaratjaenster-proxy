package org.example.Balancer;

import org.example.node.Node;

import java.util.List;

public interface LoadBalancer {

    Node next(List<Node> nodes);
}
